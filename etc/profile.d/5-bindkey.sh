if [ -v ZSH_VERSION ]
then
  bindkey "[H" beginning-of-line
  bindkey "[F" end-of-line
  bindkey "[5~" history-search-backward
  bindkey "[6~" history-search-forward
fi
