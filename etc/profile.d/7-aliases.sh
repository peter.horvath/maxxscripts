alias ls='ls --color=auto --show-control-chars'
alias file='file -szL'
alias vcl='rm -vf *~ .*~'
alias man='man'
alias git='git --no-pager'
alias vi=vim
alias view='vim -R'
