_addpath() {
  local newpath="$1"
  local flag="$2"

  if [ "$flag" = "-m" ]
  then
    if [ "$PATH" = "$newpath" ] || [ "${PATH%:$newpath}" != "$PATH" ] || [ "${PATH#$newpath:}" != "$PATH" ] || [ "${PATH%:$newpath:*}" != "$PATH" ]
    then
      return 0
    fi
  fi

  if [ "$PATH" = "" ]
  then
    PATH="$newpath"
  elif [ "$PATH" = "$newpath" ] || [ "${PATH#$newpath:}" != "$PATH" ]
  then
    true
  elif [ "${PATH%:$newpath}" != "$PATH" ]
  then
    PATH="${newpath}:${PATH%:$newpath}"
  elif [ "${PATH%:$newpath:*}" != "$PATH" ]
  then
    PATH="${newpath}:${PATH%:$newpath:*}:${PATH##*:$newpath:}"
  else
    PATH="${newpath}:${PATH}"
  fi
}

for _pathfile in /etc/paths /data/data/com.termux/files/usr/etc/paths "$HOME/.paths"
do
  if [ -f "$_pathfile" ]
  then
    while read _path2add
    do
      [ "${_path2add:0:1}" = "#" ] && continue
      _addpath "$_path2add"
    done <"$_pathfile"
  fi
done

_this="${BASH_SOURCE[0]}"
[ "${_this:0:1}" != "/" ] && _this="$(readlink -e "$_this")"
_this="${_this%/etc/profile.d/*}/bin"
_addpath "$_this"

unset _path2add
unset _pathfile
unset _addpath
