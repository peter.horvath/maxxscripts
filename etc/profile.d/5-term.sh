#if [ "${TERM#screen.}" != "$TERM" ]
#then
#  export TERM="${TERM#screen.}"
#fi

#if [ "$TERM" = xterm ] || [ "$TERM" = screen ] || [ "$TERM" = xterm-color ] || [ "$TERM" = xterm-256color ] || [ "$TERM" = "" ]
#then
#  export TERM=linux
#else
#  export TERM
#fi

if [ "$TERM" = "screen.xterm-256color" ]
then
  export TERM=xterm-256color
elif [ "${TERMUX_VERSION}" != "" ] && [ "${TERM}" = cygwin ]
then
  export TERM=linux
fi

if [ "$TERM" = "" ]
then
  export TERM=linux
fi
