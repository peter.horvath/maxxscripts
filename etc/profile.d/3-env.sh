#export PAGER="less -R -X -F"
export PAGER=most
export EDITOR=vim
# if pam_env was not executed on some reason, we do. We are also exporting everything
for _envfile in /etc/environment /data/data/com.termux/files/usr/etc/environment "$HOME/.environment"
do
  if [ -f "$_envfile" ]
  then
    . "$_envfile"
    while read r
    do
      r="${r%%#*}"
      [ "${r%=*}" = "$r" ] && continue
      [ "${r#* *=}" != "$r" ] && continue
      n="${r%%=*}"
      v="${r#*=}"
      v="${v//\*/\\\*}"
      eval "export ${n}=${v}"
    done <"$_envfile"
    unset r
    unset n
    unset v
  fi
done
unset _envfile
