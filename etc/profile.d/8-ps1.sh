if [ "$LOGNAME" = "" ] && [ "$USER" != "" ]
then
  export LOGNAME="$USER"
fi

if [ "$HOSTNAME" = "" ] && [ "$HOST" != "" ]
then
  export HOSTNAME="$HOST"
fi

if [ "$TERM" = "" ] || [ "$TERM" = "dumb" ]
then
  export _TERM=linux
else
  export _TERM="$TERM"
fi

unset PROMPT_DIRTRIM # very likely, we won't ever need if. But if yes, we should put it into an environment

if [ -v BASH_VERSION ]
then
  export PS1="$(TERM="$_TERM" setterm -background "${PROMPTCOLOR:-red}" -foreground yellow -bold on)[$(whoami)@${HOSTNAME}:\w:\t:\j:\!]$(TERM="$_TERM" setterm -background black -foreground white -bold off)\n"
else
  export PS1="$(tput bold; tput setab 4;tput setaf 3)[$(whoami)@${HOSTNAME}:%~:%*:%j:%!]$(tput sgr0;tput setab 0;tput setaf 7)
"
fi

unset _TERM

if [ "$UID" = "0" ]
then
  PS1="${PS1}# "
else
  PS1="${PS1}$ "
fi
