cmdline_escape() {
  local src="$1"
  local i
  local j
  local res=""
  local toescape='\"`()|$[]~ '\'''
  for ((i=0; i<${#src}; i++))
  do
    j="${src:$i:1}"
    if [ "$i" -eq 0 ] && [ "$j" = "~" ]
    then
      true
    elif [ "${toescape%$j*}" != "$toescape" ]
    then
      res="$res\\"
    fi
    res="$res$j"
  done
  echo -n "${res}"
}

substr_cmpl() {
  local p # thing to expand
  local p_dir # assumed directory
  local p_file # file part
  local res # iterates possible results
  local pwd_bkp="$PWD" # previous pwd
  local p_pref # prefix to the options
  local slash # empty or "/" if the option is a directory
  local n_match=0 # count of options
  local a_match # array of options, might go into COMPREPLY
  local dialog_selected # output of dialog
  local dialog_exitcode # exit code of dialog

  if [ "${COMP_WORDS[*]}" = "" ] || [ "$COMP_CWORD" = "" ]
  then
    p=""
  else
    p="${COMP_WORDS[COMP_CWORD]}"
  fi

  if [ "${p%/*}" = "$p" ]
  then
    p_dir="$PWD"
    p_file="$p"
    p_pref=""
  else
    p_file="${p##*/}"
    p_dir="${p%/*}"
    if [ "$p_dir" = "" ]
    then
      p_dir="/"
      p_pref="/"
    else
      p_pref="$p_dir/"
    fi
  fi

  if [ "$p_dir" = "~" ] || [ "${p_dir:0:2}" = "~/" ]
  then
    p_dir="${HOME}${p_dir#\~}"
  fi

  #echo >&2
  #echo "p=$p p_dir=$p_dir p_file=$p_file p_pref=$p_pref" >&2
  cd "$p_dir" >/dev/null 2>&1
  if [ "$?" -gt 0 ]
  then
    return 0
  fi

  declare -a a_match
  for res in .*"${p_file#.}"* *"$p_file"*
  do
    #echo "res=$res" >&2
    if [ -d "${p_pref}${res}" ]
    then
      slash="/"
    else
      slash=""
    fi
    a_match+=("${p_pref}${res}${slash}")
    let n_match++
  done
  if [ "$n_match" = 0 ]
  then
    true # COMPREPLY+=("$p")
  elif [ "$n_match" = 1 ]
  then
    COMPREPLY+=("$(cmdline_escape "${a_match[0]}")")
  else
    tput smcup
    {
      dialog_selected="$(dialog --no-items --no-shadow --scrollbar --keep-window --keep-tite --menu ' ' $(tput lines) $(tput cols) $(tput lines) "${a_match[@]}" 2>&1 1>&3; echo "|$?")";
    } 3>&1
    tput rmcup
    dialog_exitcode="${dialog_selected##*|}"
    dialog_selected="${dialog_selected%|*}"
    dialog_selected="$(cmdline_escape "$dialog_selected")"
    if [ "$dialog_exitcode" = 0 ]
    then
      COMPREPLY+=("$dialog_selected")
    else
      true
    fi
  fi
  #echo "${COMPREPLY[@]}" >&2
  cd "$pwd_bkp"
}

complete -r
complete -F substr_cmpl -D
#complete -F substr_cmpl -I
