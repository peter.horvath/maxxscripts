#!/data/data/com.termux/files/usr/bin/bash
set -e
shopt -s lastpipe

TERMUX_ROOT=/data/data/com.termux/files
MYPATH="$(dirname $(realpath "$0"))"
dryEcho=""

hashfix() {
  sed '1 s/^#!\/usr\/bin\//#!\/data\/data\/com.termux\/files\/usr\/bin\//'
}

if [ "$1" = "--dry" ]
then
  dryEcho="echo"
  dry=y
else
  dry=n
fi

make compile

cat "$MYPATH"/debian/maxxscripts.install "$MYPATH"/termux/install|\
sed 's/#.*$//g'|\
grep -E '^[^ ]+ [^ ]+$'|\
while read f
do
  src="${f%% *}"
  dst="${f##* }"
  if [ -d "$src" ]
  then
    find "$src" -type f|while read g
    do
      echo "$g $dst"
    done
  elif [ "${src%\**}" != "$src" ]
  then
    find $src -type f|while read g
    do
      echo "$g $dst"
    done
  else
    echo "$f"
  fi
done|while read r
do
  src_orig="${r%% *}"
  src_file="${src_orig##*/}"
  src_abspath="${MYPATH}/${src_orig}"
  dst_orig="${r##* }"
  dst_orig="${dst_orig#/}"
  do_chmod=n
  if [ "${dst_orig#etc}" != "${dst_orig}" ]
  then
    dst="usr/${dst_orig}"
  elif [ "${dst_orig#usr/bin}" != "${dst_orig}" ]
  then
    dst="$dst_orig"
    do_chmod=y
  elif [ "${dst_orig#usr/sbin}" != "${dst_orig}" ]
  then
    dst="usr/bin/${dst_orig#usr/sbin}"
    do_chmod=y
  elif [ "${dst_orig#sbin}" != "${dst_orig}" ]
  then
    dst="usr/bin/${dst_orig#sbin}"
    do_chmod=y
  elif [ "${dst_orig#bin}" != "${dst_orig}" ]
  then
    dst="usr/bin/${dst_orig#bin}"
    do_chmod=y
  else
    dst="${dst_orig}"
  fi

  dst="${TERMUX_ROOT}/${dst}"

  if [ -d "$dst" ]
  then
    dst_dir="$dst"
    dst_abspath="${dst_dir%/}/$src_file"
  else
    dst_dir="${dst%/*}"
    dst_abspath="$dst"
  fi
  if ! [ -d "$dst_dir" ]
  then
    $dryEcho mkdir -pv "$dst_dir"
  fi

  if [ "$(head -1 "$src_abspath"|while read l;do echo "${l:0:2}";done)" = "#!" ]
  then
    script_content="$(sed '1 s/^#!\/usr\/bin\//#!\/data\/data\/com.termux\/files\/usr\/bin\//' <"$src_abspath")"
    if ! [ -f "$dst_abspath" ] || [ "$script_content" != "$(cat "$dst_abspath")" ]
    then
      if [ "$dry" = n ]
      then
        echo "$script_content" >"$dst_abspath"
      fi
    fi
  else
    if ! [ -f "$dst_abspath" ] || ! diff -q "$src_abspath" "$dst_abspath" >/dev/null 2>&1
    then
      $dryEcho cp -vfa "$src_abspath" "$dst_abspath"
    fi
  fi

  if [ "${do_chmod}" = "y" ]
  then
    $dryEcho chmod -c u+x "$dst_abspath"
  fi
done

vimrc_path="${TERMUX_ROOT}/usr/share/vim/vimrc"
if ! grep -q 'source.*vimrc.local' "${vimrc_path}"
then
  echo "source ${TERMUX_ROOT}/usr/etc/vim/vimrc.local" >> "${vimrc_path}"
fi

if ! [ -f "${TERMUX_ROOT}/usr/etc/paths" ]
then
  $dryEcho cp -vfa termux/paths "${TERMUX_ROOT}/usr/etc/paths"
fi
