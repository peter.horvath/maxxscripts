#!/usr/bin/bash
declare -A DIRECT

DIRECT["AF-AM"]="A verlängerung"
DIRECT["AF-MF"]="A-M konverter AM"
#DIRECT["AF-MM"]="A-M fater konverter"
DIRECT["AM-AM"]="A link"
DIRECT["AM-CF"]="C-A konverter"
DIRECT["AM-CM"]="C ladekabel"
DIRECT["AM-MF"]="A-M konverter AF"
DIRECT["AM-MM"]="M ladekabel"
DIRECT["CF-CF"]="C expander"
DIRECT["CF-MM"]="C-M adapter"
DIRECT["CM-CM"]="C laptop ladekabel"

# Largest removable subset: AM-MM CF-CF AF-AM AM-AM AM-CM

DEBUG=2

debug() {
  if [ "$1" -le "$DEBUG" ]
  then
    shift
    echo "$@"
  else
    return 0
  fi
}

while [ "$*" != "" ]
do
  if [ "$1" == "--keys" ]
  then
    debug 0 "${!DIRECT[@]}"
    shift
    exit 0
  elif [ "$1" == "--unset" ]
  then
    shift
    unset DIRECT[$1]
    shift
  elif [ "$1" == "--debug" ]
  then
    shift
    DEBUG="$1"
    shift
  fi
done

# making reverses
for i in "${!DIRECT[@]}"
do
  i_from="${i%-*}"
  i_to="${i#*-}"
  [[ "$i_from" < "$i_to" ]] && DIRECT["${i_to}-${i_from}"]="${DIRECT[$i]}"
done

while [ "$found" != "n" ]
do
  debug 3 "--- search"
  found=n
  for i in "${!DIRECT[@]}"
  do
    i_name="${DIRECT[$i]}"
    i_from="${i%-*}"
    i_to="${i#*-}"
    for j in "${!DIRECT[@]}"
    do
      [ "$i" = "$j" ] && continue
      j_name="${DIRECT[$j]}"
      j_from="${j%-*}"
      j_to="${j#*-}"
      [ "${i_to:0:1}" != "${j_from:0:1}" ] && continue
      [ "${i_to:1:1}" = "${j_from:1:1}" ] && continue
      new_name="$i_name ($i) - $j_name ($j)"
      debug 3 -n "$new_name: "

      is_dupe=n
      for k in "${!DIRECT[@]}"
      do
        v="${DIRECT[$k]}"
        if [ "${new_name%$v*$v*}" != "$new_name" ]
        then
          debug 3 "Duplicated solution, no way"
          is_dupe=y
          break
        fi
      done
      [ "$is_dupe" = y ] && continue

      if [[ "$i_from" < "$j_to" ]]
      then
        c="$i_from-$j_to"
      else
        c="$j_to-$i_from"
      fi
      debug 3 -n "$c "
      if [ "${DIRECT[$c]}" != "" ]
      then
        debug 3 "We already have"
      else
        debug 3 "New hit!"
        DIRECT["$c"]="($new_name)"
        found=y
      fi
    done
  done
done

debug 2 "--- result"
for i in {A,C,M}{F,M}
do
  for j in {A,C,M}{F,M}
  do
    [[ "$i" > "$j" ]] && continue
    c="$i-$j"
    if [ "${DIRECT[$c]}" != "" ]
    then
      debug 0 "$c - ${DIRECT[$c]}"
    else
      debug 0 "$c - we still miss you"
    fi
  done
done
