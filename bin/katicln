#!/usr/bin/bash
#shopt -s lastpipe

export dodebug=n
export reallydel=n

die() {
  echo "$@" >&2
  exit 1
}

usage() {
  echo "--help: shows this
--debug: show debug info
--reallydel: really delete the files"
  exit 0
}

test_int() {
  [[ "$1" =~ ^[0-9]+$ ]] && return 0
  die "\'$1\' is not a valid integer"
}

debug() {
  [ "$dodebug" = y ] && echo "$@" >&2
  return 0
}

while [ "$*" != "" ]
do
  if [ "$1" = "--help" ]
  then
    usage
  elif [ "$1" = "--debug" ]
  then
    export dodebug=y
  elif [ "$1" = "--reallydel" ]
  then
    export reallydel=y
  else
   die "invalid argument, use --help"
  fi
  shift
done

export ALLOCATED_SIZE="$(du -bs .| awk '{print $1}')"
debug "ALLOCATED_SIZE=$ALLOCATED_SIZE"
export FREE_SIZE_K="$(df -k .|grep ^/|head -1|awk '{print $4}')"
debug "FREE_SIZE_K=$FREE_SIZE_K"
export NOW="$(date +%s)"
debug "NOW=$NOW"

test_int "$ALLOCATED_SIZE"
test_int "$FREE_SIZE_K"
test_int "$NOW"

export FREE_SIZE="$[FREE_SIZE_K<<10]"
debug "FREE_SIZE=$FREE_SIZE"
export FREE_WITHOUT_DIR="$[FREE_SIZE+ALLOCATED_SIZE]"
debug "FREE_WITHOUT_DIR=$FREE_WITHOUT_DIR"
export MUST_FREE_AFTER="$[FREE_WITHOUT_DIR>>1]"
debug "MUST_FREE_AFTER=$MUST_FREE_AFTER"
export MUST_DELETE="$[MUST_FREE_AFTER-FREE_SIZE]"
debug "MUST_DELETE=$MUST_DELETE"

deletedSize=0
find -type f -printf '%T@ %s %p\n'|while read l
do
  ts="${l%% *}"
  ts="${ts%.*}"
  l="${l#* }"
  size="${l%% *}"
  l="${l#* }"
  name="$l"
  if [ "$ts" -ge "$NOW" ]
  then
    age=0
  else
    age=$[NOW-ts]
  fi
  ageDay=$[age/86400]
  sizeBlocks=$[size/4096]
  goodness=$[ageDay*sizeBlocks]
  echo "$size $goodness $name"
done|sort -n -k +2 -r|while read l
do
  size="${l%% *}"
  l="${l#* }"
  goodness="${l%% *}"
  l="${l#* }"
  name="$l"
  logline="size: $size deletedSize: $deletedSize goodness: $goodness name: $name"
  if [ "$deletedSize" -le "$MUST_DELETE" ]
  then
    debug "$logline DELETE"
    deletedSize=$[deletedSize+size]
    [ "$reallydel" = y ] && rm -vf "$name"
  else
    debug "$logline REMAIN"
  fi
done
