#!/usr/bin/bash

die() {
  echo "$0: $*" >&2
  exit 1
}

debug() {
  [ "$_DEBUG" != "" ] && echo "$*" >&2
}

log2() {
  local i="$1"
  local j=0
  while [ "$i" -gt 0 ]
  do
    let 'i>>=1'
    let j++
  done
  echo "$j"
}

if [ "$1" = "" ]
then
  tgtDir="$PWD"
else
  tgtDir="$1"
fi

if ! [ -d "$tgtDir" ]
then
  mkdir -pv "$tgtDir" || die "can not create target directory $tgtDir"
fi

if [ "${_dateNow}" == "" ]
then
  dateNow="$(date --utc +%Y%m%d)"
else
  dateNow="$_dateNow"
fi

dayNow=$[$(date --utc -d ${dateNow} +%s)/86400]
debug dateNow: $dateNow dayNow: $dayNow

lastDir=""
prevLastDir=""

declare -a catLast

ls -r "$tgtDir"|while read n
do
  if [[ "$n" =~ ^[0-9]{8}$ ]]
  then
    debug "path: $n"
  else
    debug "$n is an invalid directory name"
    continue
  fi

  if ! [ -d "$tgtDir/$n" ]
  then
    debug "$tgtDir/$n is not a directory"
    continue
  fi

  day=$[$(date --utc -d "$n" +%s)/86400]
  debug "day: $day"

  if let 'day>dateNow'
  then
    debug "$n is in the future"
    continue
  fi

  if [ "$lastDir" = "" ]
  then
    lastDir="$n"
    debug "last backed up directory: $lastDir"
  elif [ "$prevLastDir" == "" ]
  then
    prevLastDir="$n"
    debug "last to prev backed up directory: $prevLastDir"
  fi

  category=$(log2 $[dayNow-day])
  debug "category: $category"

  cl="${catLast[$category]}"
  if [ "$cl" = "" ]
  then
    debug "is first in its category"
  else
    debug "last in its category is: $cl, we delete it"
    rm -rvf "$tgtDir/$cl"
  fi
  catLast[$category]="$n"
  debug "last in its category is now: $n"
done

if [ -d "$tgtDir/$dateNow" ]
then
  debug "todays directory $dateNow exists, so we try to reset the backup from prevLast one"
  if [ "$prevLastDir" = "" ]
  then
    debug "  ..but it does not exist, so we do nothing"
  else
    debug "  ..it exists, so we do an rsync"
    rsync -vaH --delete "$tgtDir/$prevLastDir/" "$tgtDir/$lastDir/"
  fi
else
  debug "todays directory does not exist..."
  if [ "$lastDir" == "" ]
  then
    debug "  ...and also no lastDir exists, do we create an empty directory"
    mkdir -pv "$tgtDir/$dateNow"
  else
    debug "  ...but lastDir $lastDir exists, so we create a hardlinked copy"
    cp -vfa --link "$tgtDir/$lastDir" "$tgtDir/$dateNow"
  fi
fi

echo "$tgtDir/$dateNow"
