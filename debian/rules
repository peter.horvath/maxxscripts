#!/usr/bin/make -f
SHELL:=/bin/bash

-include $(dir $(realpath $(lastword $(MAKEFILE_LIST))))/../../Makefile.inc

.PHONY: clean
clean:
	make clean
	rm -rvf ../maxxscripts.substvars ../maxxscripts_* debian/files debian/maxxscripts etc/profile.d/dir_colors
	dh_clean

.PHONY: binary
binary: binary-indep

.PHONY: binary-indep
binary-indep:
	dh_gencontrol
	mkdir -pv debian/maxxscripts/usr/share/doc/maxxscripts
	gzip -9v <debian/changelog >debian/maxxscripts/usr/share/doc/maxxscripts/changelog.Debian.gz
	cp -vfa debian/{conffiles,postrm} debian/maxxscripts/DEBIAN
	chmod 755 debian/maxxscripts/usr/lib/nagios/plugins/*
	chown root:root -c debian/maxxscripts/{bin,usr/bin}/*
	chmod 755 -c debian/maxxscripts/{bin,usr/bin}/* debian/postrm
	dh_builddeb

.PHONY: build-arch binary-arch
build-arch binary-arch:
	@echo There is no $@ target here.

.PHONY: build-indep
build-indep: version
	$(MAKE) compile

.PHONY: build
build: build-indep
	dh_install
	dh_link

LEFTPAREN=(
RIGHTPAREN=)
VERSION=$(shell head -1 debian/changelog|sed -e 's/^.*$(LEFTPAREN)//g' -e 's/$(RIGHTPAREN).*$$//g')

.PHONY: version
version:
	echo $(VERSION)-$(shell git log -1 --oneline|sed -e 's/ .*$$//g') > etc/maxxscripts/version

.PHONY: debsrc
debsrc:
	[ -d target/srcdeb ] || mkdir -pv target/srcdeb
	rsync -vaH --delete --exclude-from=.gitignore --exclude=.git --exclude=.gitignore ./ target/srcdeb/
	#cp -vfa bin debian etc LICENSE.md Makefile target/srcdeb
	cd .. && dpkg-source -b maxxscripts/target/srcdeb
	dpkg-genchanges -S >../maxxscripts_$(VERSION)_all.changes
	debsign -k$$(cat ../keyid.txt) ../maxxscripts_$(VERSION)_all.changes

.PHONY: srcdeb
srcdeb: debsrc

.PHONY: src-deb
src-deb: debsrc

.PHONY: deb-src
deb-src: debsrc

.PHONY: upload
upload:
	cd .. && dput -u dockerclub maxxscripts_$(VERSION)_all.changes

.PHONY: deploy
deploy:
ifndef DEPLOY_URL
	$(error No deploy URL, consider using ../../Makefile.inc)
endif
	curl -v --netrc-optional --form 'package=@../maxxscripts_$(VERSION)_all.deb' $(DEPLOY_URL)
