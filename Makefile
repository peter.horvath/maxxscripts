#!/usr/bin/make -f
## TODO: non-deb builds

.PHONY: default
default:
	@cat TARGETS.txt

.PHONY: debian
debian:
	debian/rules clean build
	fakeroot debian/rules binary
	debian/rules debsrc upload

.PHONY: compile
compile:
	dircolors -b etc/maxxscripts/LS_COLORS >etc/profile.d/3-dir_colors.sh
	debian/rules version

.PHONY: srctgz
srctgz:
	tar -cvf - $$(git ls-files) --transform='s/^/maxxscripts\//'|gzip -9v - >../maxxscripts.src.tgz

.PHONY: clean
clean:
	rm -rvf target
