#!/bin/bash
MYPATH="$(realpath $(dirname "$0"))"
dryEcho=""

if [ "$1" = "--dry" ]
then
  dryEcho="echo"
fi

pushd "$MYPATH"

make compile

cat debian/maxxscripts.install cygwin/cygwin.install |\
sed 's/#.*$//g' |\
grep -E '^[^ ]+ [^ ]+$' |\
while read r
do
  r="${r%%[[:cntrl:]]}"
  from="${r%% *}"
  to="${r##* }"
  if [ "$to" = "/etc/vim" ]
  then
    continue
  fi
  if ! [ -d "$to" ]
  then
    $dryEcho mkdir -pv "$to"
  fi
  $dryEcho cp -vfa $from "$to"
done
