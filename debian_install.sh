#!/bin/bash -x
MYPATH="$(realpath $(dirname "$0"))"
cd "$MYPATH"
VERSION=$(head -1 debian/changelog|sed -e 's/^.*(//g' -e 's/).*$//g')
if [ "$UID" = 0 ]
then
  FAKEROOT=
  SUDO=
else
  FAKEROOT=fakeroot
  SUDO=sudo
fi
$FAKEROOT debian/rules build
DEBPATH="../maxxscripts_${VERSION}_all.deb"
$SUDO rm -vf "$DEBPATH"
$FAKEROOT debian/rules binary
$SUDO dpkg --force-overwrite -i "$DEBPATH"
